<?php get_header(); ?>

<?php
$categoryID = get_query_var('cat');
$category = get_category($categoryID);

$categorySlug = $category->slug;
$categoryName = $category->name;
$categoryDesc = $category->description;

// subcategories
$subCategory = get_categories(array('child_of' => $categoryID, 'hide_empty' => 0));

// navigate categories
$moreCategory = get_categories(array('parent' => 5, 'hide_empty' => 0));

$curr = 0;
$ncat = sizeof($moreCategory);

// calculate prev and next category
foreach ($moreCategory as $pos => $cat) {
    if ($cat->cat_ID == $categoryID) { $curr = $pos;  }
}

$prev = 0;
if ($curr == 0) { $prev = $ncat-1; } 
else { $prev = $curr-1; }

$next = 0;
if ($curr == $ncat-1) { $next = 0; } 
else { $next = $curr+1; }
?>

    <div class="breadcrumb">
        <div class="content"><img src="<?php echo get_template_path(); ?>/img/backarrow.png" class="backarrow"><a href="<?php echo get_main_path("/"); ?>">Iniciativas</a> / <span><?php echo $categoryName; ?></span></div>
    </div>
    <div class="arrows-movil">
        <a rel="prev" href="<?php echo get_main_path(); ?>/iniciativas/<?php echo $moreCategory[$prev]->slug; ?>/" class="arrowprev"><img src="<?php echo get_template_path(); ?>/img/backarrow-movil.gif">ANTERIOR</a>
        <a rel="next" href="<?php echo get_main_path(); ?>/iniciativas/<?php echo $moreCategory[$next]->slug; ?>/" class="arrownext">SIGUIENTE<img src="<?php echo get_template_path(); ?>/img/nextarrow-movil.gif"></a>
    </div>
    <div class="contenido">
        
        <!-- banner -->
        <div class="banner">
            <img src="<?php echo get_template_path(); ?>/img/<?php echo $categorySlug; ?>-banner.jpg" width="1022" height="634" usemap="#Map">
            
            <div class="texto">
                <h1><?php echo $categoryName; ?></h1>
                <h2><?php echo $categoryDesc; ?></h2>
            </div>
        </div>
        <!--div class="arrows">
	        <a rel="prev" href="<?php echo get_main_path(); ?>/iniciativas/<?php echo $moreCategory[$prev]->slug; ?>/" class="arrowprev"><img src="<?php echo get_template_path(); ?>/img/arrowprev.gif"></a>
	        <a rel="next" href="<?php echo get_main_path(); ?>/iniciativas/<?php echo $moreCategory[$next]->slug; ?>/" class="arrownext"><img src="<?php echo get_template_path(); ?>/img/arrownext.gif"></a>
        </div-->
        <?php foreach ($subCategory as $sub) : ?>
        <!-- show subcategories -->
        
        <?php $posts = get_posts(array('posts_per_page' => -1, 'post_type' => 'post', 'category'=> $sub->cat_ID, 'meta_key' => 'destacado', 'meta_value' => 'si')); ?>
        <?php if ($posts): ?>
        <div class="header-block">
            <h1> <?php echo $sub->name . ' de ' . $categoryName; ?></h1>
            <div class="orden">Ordenar por: <a href="#" class="fecha selected" data-sort-by="fecha">FECHA</a><a href="#" class="relevancia" data-sort-by="visto">RELEVANCIA</a></div>
            <div class="clear"></div>
        </div>
        <div class="orden-movil">
        	<h1><?php echo $sub->name . ' de ' . $categoryName; ?></h1>
        	<div class="select">
                <div class="pulldown">
                    <div class="txtpull"><span>ORDENAR POR</span></div>
                    <select name="ordensel">
                        <option value="fecha">FECHA</option>
                        <option value="visto">RELEVANCIA</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="casos">
            <div class="container">
                <?php foreach( $posts as $post ): setup_postdata( $post ); ?>
                <?php 
                    // count views 
                    $counter = get_post_meta(get_the_ID(), '_post_views'); 
                    $views = 0 + $counter[0];
                ?>
                <div class="item">
                    <a href="<?php echo get_relative_permalink(); ?>"><img src="<?php echo sanitize_path(get_field('foto_miniatura')); ?>" class="img-zoom"></a>
                    <a href="<?php echo get_relative_permalink(); ?>"><div class="info">
                        <span class="fecha"><?php echo get_post_time(); ?></span>
                        <span class="categoria"><?php echo $categoryName; ?></span><br>
                        <span class="titulo"><?php echo wp_trim_words(strip_tags( get_the_title()), 9, '...' ); ?></span><br>
                        <span class="visto"><?php echo $views; ?></span><span class="compartido"><?php echo (pssc_facebook(get_the_ID()) + pssc_gplus(get_the_ID())); ?></span>
                    </div></a>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="clear"></div>
        </div>
        <?php endif; ?>
        
        <?php endforeach; ?>
        
        
     
        <div class="arrows-movil" style="border-top: solid 1px #dadada; margin: 40px 0 0 0;">
	        <a rel="prev" href="<?php echo get_main_path(); ?>/iniciativas/<?php echo $moreCategory[$prev]->slug; ?>/" class="arrowprev"><img src="<?php echo get_template_path(); ?>/img/backarrow-movil.gif">ANTERIOR</a>
	        <a rel="next" href="<?php echo get_main_path(); ?>/iniciativas/<?php echo $moreCategory[$next]->slug; ?>/" class="arrownext">SIGUIENTE<img src="<?php echo get_template_path(); ?>/img/nextarrow-movil.gif"></a>
	    </div>
        <div class="clear"></div>
    </div>

<?php get_footer(); ?>
