<?php get_header(); ?>

<?php
$subCategory = get_the_category();

foreach ($subCategory as $subCat) {
    if ($subCat->parent == 5) { $sub = $subCat; }
    if ($subCat->parent == 0) { $cat = $subCat; }
}
foreach ($subCategory as $subCat) {
    if ($subCat->parent == $sub->cat_ID) { $sec = $subCat; }
}

$postID = get_the_ID();
$postKEYWORD = get_field('keyword');

$edicionTitle = "";
$edicionLink = "";
$edicionID = 0;

// get edicion base
if ($postKEYWORD != "") {
    $edicion = get_posts(array(
        'numberposts'	=> -1,
        'post_type'		=> 'post',
        'meta_query'	=> array(
            'relation'		=> 'AND',
            array(
                'key'	 	=> 'keyword',
                'value'	  	=> $postKEYWORD,
                'compare' 	=> '=',
            ),
            array(
                'key'	  	=> 'destacado',
                'value'	  	=> 'si',
                'compare' 	=> '=',
            ),
        ),
    ));
    $edicionTitle = $edicion[0]->post_title;
    $edicionLink = $edicion[0]->post_name;
    $edicionID = $edicion[0]->ID;
}

$progQueCat = new WP_Query( array('meta_key' => 'keyword', 'meta_value' => $postKEYWORD));
$arr = array();
$cur = 0;
$tmp = 0;

while ($progQueCat->have_posts()) {
    $progQueCat->the_post();

    $arr[] = get_relative_permalink();
    if ($postID == get_the_ID()) { $cur = $tmp; }
    $tmp++;
}
wp_reset_query();

if ($cur == $tmp-1) { $nextLink = $arr[0]; }
else { $nextLink = $arr[$cur+1]; }

if ($cur == 0) { $prevLink = $arr[$tmp-1]; }
else { $prevLink = $arr[$cur-1]; }
?>

	<div class="breadcrumb">
        <div class="content">
        	<img src="<?php echo get_template_path(); ?>/img/backarrow.png" class="backarrow"><a href="<?php echo get_main_path('/'); ?>"><?php echo $cat->name; ?></a> /
        	<a href="<?php echo get_main_path('iniciativas/'.$sub->slug.'/'); ?>"><?php echo $sub->name; ?></a> /
            <?php if ($edicionLink != "" && $postID != $edicionID) {?>
            <a href="<?php echo get_main_path($edicionLink); ?>/"><?php echo $edicionTitle; ?></a> /
            <?php } ?>
        	<span><?php echo the_title(); ?></span>
        </div>
    </div>

    <?php if ($postKEYWORD != "" && $tmp > 1) : ?>
    <div class="arrows-movil" style="border-top: solid 1px #dadada; border-bottom: solid 1px #dadada; margin: 0;">
        <a rel="prev" href="<?php echo $prevLink; ?>/" class="arrowprev"><img src="<?php echo get_template_path(); ?>/img/backarrow-movil.gif">ANTERIOR</a>
        <a rel="next" href="<?php echo $nextLink; ?>/" class="arrownext">SIGUIENTE<img src="<?php echo get_template_path(); ?>/img/nextarrow-movil.gif"></a>
    </div>
    <?php endif; ?>

    <div class="box-wide">
        <div class="contenido-box">
            <h1><?php the_title(); ?></h1>
            <h2><?php the_excerpt(); ?></h2>
            <div class="content-banner2">
                <div class="banner2">
                    <ul>
                        <li>
                            <?php if(get_field('contenido_portada') == 'Imagen'){ ?>
                            <img  style="width:100%;" src="<?php echo sanitize_path(get_field('foto_principal')); ?>">
                            <?php } else {?>
                            <?php echo fmtYoutubeFrame(get_field('video')); ?>
                            <?php } ?>
                       </li>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>

            <?php if ($postKEYWORD != "" && $tmp > 1) : ?>
            <a rel="prev" href="<?php echo $prevLink; ?>" class="arrowprev"><img src="<?php echo get_template_path(); ?>/img/arrowprev.gif"></a>
        	<a rel="next" href="<?php echo $nextLink; ?>" class="arrownext"><img src="<?php echo get_template_path(); ?>/img/arrownext.gif"></a>
            <?php endif; ?>
        </div>
    </div>

    <div class="contenido-post">
        <div class="cols col2">
            <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>
            <?php
				$content = apply_filters( 'the_content', get_the_content() );
				$content = str_replace( ']]>', ']]&gt;', $content );
				echo sanitize_path($content);
			?>
            <?php endwhile; ?>
            <?php endif; ?>

        </div>
    </div>

    <?php $images = get_field('galeria'); ?>
    <?php if ( $images ):  $j=0; ?>

    <div class="galeria">
        <h1><img src="<?php echo get_template_path(); ?>/img/icon-gallery.gif">Galería del Programa</h1>
        <div class="carousel-galeria">
            	<?php foreach( $images as $image ): $j++; ?>
                    <div><a href="<?php echo sanitize_path($image['url']); ?>" class="<?php echo $j; ?>"><img src="<?php echo sanitize_path($image['url']); ?>" alt="<?php echo $image['alt']; ?>" class="img"></a></div>
            	<?php endforeach; ?>
        </div>

        <div class="controls">
            <a href="#" class="arrowprev2"><img src="<?php echo get_template_path(); ?>/img/arrowleft2.gif"></a>
            <a href="#" class="arrownext2"><img src="<?php echo get_template_path(); ?>/img/arrowright2.gif"></a>
            <div class="counter"><span class="first">1</span>/<span class="second">7</span></div>
        </div>
    </div>
    <?php endif; ?>
    <div class="clear"></div>
    <div class="share-top">
        <div class="content">
            <img src="<?php echo get_template_path(); ?>/img/icon-voice.png" class="icon-voice"><span>Ayúdanos y cuéntales a tus amigos qué te pareció. ¡Gracias!</span>
            <div class="social">
                <a href="#" class="fb" data-url="<?php echo "http://www.lan.com".get_relative_permalink(); ?>" data-key="<?php echo $postKEYWORD; ?>" data-title="<?php the_title(); ?>" data-img="<?php echo sanitize_path(get_field('foto_miniatura')); ?>" data-text="<?php echo strip_tags(get_the_excerpt()); ?>"><img src="<?php echo get_template_path(); ?>/img/icon-fb.png"></a>
                <a href="https://plus.google.com/share?url=<?php echo "http://www.lan.com".get_relative_permalink(); ?>" class="go" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                <img src="<?php echo get_template_path(); ?>/img/icon-g.png" alt="Share on Google+"></a>
            </div>
        </div>
    </div>

    <?php if ($postKEYWORD != "") : ?>
    <?php $progQue = new WP_Query( array('meta_key' => 'keyword', 'meta_value' => $postKEYWORD)); ?>

    <?php if ($progQue->have_posts()): ?>
    <div class="box-wide margin-bottom-box">
        <div class="contenido-box contenido-post">
        <div class="header-block">
            <h1>Ediciones de <?php echo $edicionTitle; ?></h1>
            <div class="orden">Ordenar por: <a href="#" class="fecha selected" data-sort-by="fecha">FECHA</a><a href="#" class="relevancia" data-sort-by="visto">RELEVANCIA</a></div>
            <div class="clear"></div>
        </div>
        <div class="orden-movil">
        	<h1>Ediciones de <?php echo $edicionTitle; ?></h1>
        	<div class="select">
                <div class="pulldown">
                    <div class="txtpull"><span>ORDENAR POR</span></div>
                    <select name="ordensel">
                        <option value="fecha">FECHA</option>
                        <option value="visto">RELEVANCIA</option>
                    </select>
                </div>
            </div>
        </div>
            <div class="casos" style="text-align: left;">
                <div class="container">

                    <?php while ($progQue->have_posts()) : $progQue->the_post(); ?>
                    <?php if(get_the_ID() != $postID): ?>
                    <?php
                        // count views
                        $counter = get_post_meta(get_the_ID(), '_post_views');
                        $views = 0 + $counter[0];
                    ?>

                   <div class="item">
	                    <a href="<?php echo get_relative_permalink(); ?>"><img src="<?php echo sanitize_path(get_field('foto_miniatura')); ?>" class="img-zoom"></a>
	                    <a href="<?php echo get_relative_permalink(); ?>">
                            <div class="info">
    	                        <span class="fecha"><?php echo get_post_time(); ?></span>
    	                        <span class="categoria"><?php echo $edicionTitle; ?></span><br>
    	                        <span class="titulo"><?php the_title(); ?></span><br>
    	                        <span class="visto"><?php echo $views; ?></span><span class="compartido"><?php echo get_scp_facebook(); ?></span>
    	                    </div>
                        </a>
	                </div>

                    <?php endif; ?>
                    <?php endwhile; ?>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
	<?php endif; ?>

    <?php else: ?>
    <!-- related -->
    <?php $progQue = new WP_Query( array('meta_key' => 'destacado', 'meta_value' => 'si', 'cat' => $sec->cat_ID, 'post__not_in' => array($postID), 'posts_per_page' => 3)); ?>
    <?php if ($progQue->have_posts()): ?>
    <div class="box-wide margin-bottom-box">
        <div class="contenido-box contenido-post mas-programas">
        <div class="header-block">
            <h2>Más <?php echo $sec->name;?> de <?php echo $sub->name; ?></h2>
            <div class="clear"></div>
        </div>
            <div class="casos" style="text-align: left;">
                <div class="container">

                    <?php while ($progQue->have_posts()) : $progQue->the_post(); ?>
                    <?php if(get_the_ID() != $postID): ?>
                    <?php
                        // count views
                        $counter = get_post_meta(get_the_ID(), '_post_views');
                        $views = 0 + $counter[0];
                    ?>

                   <div class="item">
	                    <a href="<?php echo get_relative_permalink(); ?>"><img src="<?php echo sanitize_path(get_field('foto_miniatura')); ?>" class="img-zoom"></a>
                        <a href="<?php echo get_relative_permalink(); ?>">
    	                    <div class="info">
    	                        <span class="fecha"><?php echo get_post_time(); ?></span>
    	                        <span class="categoria"><?php echo $sub->name; ?></span><br>
    	                        <span class="titulo"><?php the_title(); ?></span><br>
    	                        <span class="visto"><?php echo $views; ?></span><span class="compartido"><?php echo (pssc_facebook(get_the_ID()) + pssc_gplus(get_the_ID())); ?></span>
    	                    </div>
                        </a>
	                </div>

                    <?php endif; ?>
                    <?php endwhile; ?>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
	<?php endif; ?>
    <?php endif; ?>
    <div class="clear"></div>
    <?php if ($postKEYWORD != "" && $tmp > 1) : ?>
    <div class="arrows-movil" style="border-top: solid 1px #dadada; margin: 40px 0 0 0;">
	   <a rel="prev" href="<?php echo $prevLink; ?>/" class="arrowprev"><img src="<?php echo get_template_path(); ?>/img/backarrow-movil.gif">ANTERIOR</a>
	   <a rel="next" href="<?php echo $nextLink; ?>/" class="arrownext">SIGUIENTE<img src="<?php echo get_template_path(); ?>/img/nextarrow-movil.gif"></a>
	</div>
    <?php endif; ?>
<?php get_footer(); ?>
