<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo esc_url( get_template_path() ); ?>/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo esc_url( get_template_path() ); ?>/owl-carousel/owl.theme.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_template_path() ); ?>/css/normalize.css">
    <link rel="stylesheet" href="<?php echo esc_url( get_template_path() ); ?>/style.css">
    <link rel='stylesheet' id='dry_awp_theme_style-css'  href='<?php echo get_main_path(); ?>/wp-content/plugins/advanced-wp-columns/assets/css/awp-columns.css?ver=4.2.2' type='text/css' media='all' />
	<script>(function(){document.documentElement.className='js'})();</script>
    <?php 
    $blog_title = get_bloginfo('name'); 
    if (is_home()) {
        $blog_title = get_bloginfo('name') . " | " . get_bloginfo('description');
    }
    if (is_single()) {
        $blog_title = get_the_title();
    }
    if (is_category()) {
        $blog_title = single_cat_title("",false) . " archive | " . get_bloginfo('name');
    }
    if (is_page()) {
        $blog_title = get_the_title();
    } 
    ?>
	<?php wp_head(); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<?php echo esc_url( get_template_path() ); ?>/js/main.js"></script>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NKLV3W" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NKLV3W');</script>
<!-- End Google Tag Manager -->

<div class="main">
	<div>
		<div class="header" style="	background-color: #2B2E73;">
			<div class="content">
				<a href="#" class="btnmenu">
					<img src="<?php echo esc_url( get_template_path() ); ?>/img/btnmenu.png" class="normal">
					<img src="<?php echo esc_url( get_template_path() ); ?>/img/btnmenu-on.png" class="over">
				</a>
				<a href="<?php echo get_main_path( '/' ); ?>" class="logo"><img src="<?php echo esc_url( get_template_path() ); ?>/img/logo.png"></a>
				<a href="http://www.latam.com" class="logolan"><img style ="margin-top: -15px;" src="<?php echo esc_url( get_template_path() ); ?>/img/logo-latam-mu.png"></a>
			</div>
		</div>
		<div class="menu-expanded">
			<div class="content">
				<div class="bg">
					<ul>
						<li><a href="<?php echo get_main_path( '/' ); ?>">inicio</a></li>
						<li class="ini">Un mundo mejor</li>
						<li><a href="<?php echo get_main_path( 'iniciativas/turismo-sostenible/' ); ?>" class="subm">Turismo sostenible</a></li>
						<li><a href="<?php echo get_main_path( 'iniciativas/comunidad/' ); ?>" class="subm">Comunidad</a></li>
						<li><a href="<?php echo get_main_path( 'iniciativas/cambio-climatico/' ); ?>" class="subm">Cambio Climático</a></li>
						<li><a href="<?php echo get_main_path( 'galeria/' ); ?>">GALERÍA</a></li>
						<li><a href="<?php echo get_main_path( 'galeria/' ); ?>" class="subm">Fotografías</a></li>
						<li><a href="<?php echo get_main_path( 'galeria/' ); ?>" class="subm">Videos</a></li>
						<li><a href="<?php echo get_main_path( 'contacto/' ); ?>" class="subm">Contáctanos</a></li>
					</ul>
					<!--<div class="bottom">
						<a href="https://www.facebook.com/lanenperu" target="_blank" class="fb"><img src="<?php echo esc_url( get_template_path() ); ?>/img/fb.png"></a>
						<a href="https://www.youtube.com/user/LANenPeru" target="_blank" class="yt"><img src="<?php echo esc_url( get_template_path() ); ?>/img/yt.png"></a>
						<a href="https://plus.google.com/u/1/+LANenPeru/posts" target="_blank" class="go"><img src="<?php echo esc_url( get_template_path() ); ?>/img/go.png"></a>
						<div class="copy">&copy;<?php echo date("Y"); ?> NUESTRO PRÓXIMO DESTINO UN<br>MUNDO MEJOR, una división de Lan Perú, Inc.</div>
					</div>-->
				</div>
				<a href="#" class="btnmenu"><img src="<?php echo esc_url( get_template_path() ); ?>/img/btnmenu-on.png"></a>
			</div>
		</div>
