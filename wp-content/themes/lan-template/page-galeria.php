<?php get_header(); ?>


    <div class="popup-galeria">
        <div class="bg"></div>
        <div class="galeria">
            <div class="carousel-galeria owl-carousel" id="owl-gallery">
                <div class="info">
                    <p><?php the_title(); ?></p>
                </div>       
            </div>
            <div class="controls">
                <a href="#" class="arrowprev3"><img src="<?php echo get_template_path(); ?>/img/arrowleft2.gif"></a>
                <a href="#" class="arrownext3"><img src="<?php echo get_template_path(); ?>/img/arrowright2.gif"></a>
                <div class="counter"><span class="first">1</span>/<span class="second">7</span></div>
            </div>
            <a href="#" class="regresar"><img src="<?php echo get_template_path(); ?>/img/backarrow2.png">Regresar</a>
        </div>
    </div>

    <div class="page-galeria">
        <div class="contenido">
            <div class="left">
                <div class="titulo">
                    <h1>GALERÍA</h1>
                </div>
                <div class="menu-lat">
                    <ul>
                        <li><a href="#" data-filter-by="*" class="selected"><span>Todos</span></a></li>
                        <li><a href="#" data-filter-by=".turismo-sostenible"><span>Turismo sostenible</span></a></li>
                        <li><a href="#" data-filter-by=".comunidad"><span>Comunidad</span></a></li>
                        <li><a href="#" data-filter-by=".cambio-climatico"><span>Cambio climático</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="right">
            	<div class="titulo">
                    <h1>GALERÍA</h1>
                </div>
                <div class="headers">
                    <div class="links">
                        <ul>
	                        <li><a href="#" data-filter-by="*" class="selected"><span>Todos</span></a></li>
                            <li><a href="#" data-filter-by=".videos">Videos</a></li>
                            <li><a href="#" data-filter-by=".fotos">Fotografías</a></li>
                        </ul>
                    </div>
                    <div class="orden">
                        <ul>
                            <li>Ordenar por:</li>
                            <li><a href="#" class="fecha" data-sort-by="fecha">fecha</a></li>
                            <li><a href="#" class="relevancia" data-sort-by="visto">relevancia</a></li>
                        </ul>
                    </div>
                </div>
                <div class="headers-movil">
                    <div class="links">
                        <ul>
	                        <li><a href="#" data-filter-by="*" class="selected"><span>Todos</span></a></li>
	                        <li><a href="#" data-filter-by=".turismo-sostenible"><span>Turismo sostenible</span></a></li>
	                        <li><a href="#" data-filter-by=".comunidad"><span>Comunidad</span></a></li>
	                        <li><a href="#" data-filter-by=".cambio-climatico"><span>Cambio climático</span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="orden-movil">
		        	<div class="select" style="width: 75%; float: left">
		                <div class="pulldown">
		                    <div class="txtpull"><span>TODOS</span></div>
		                    <select name="ordensel1">
		                        <option value=".fotos">FOTOS</option>
		                        <option value=".videos">VIDEOS</option>
		                    </select>
		                </div>
		            </div>
		            <div class="select" style="width: 25%; float: left;">
		                <div class="pulldown">
		                    <div class="txtpull"><span>ORDENAR POR</span></div>
		                    <select name="ordensel2">
		                        <option value="fecha">FECHA</option>
		                        <option value="visto">RELEVANCIA</option>
		                    </select>
		                </div>
		            </div>
		            <div class="clear"></div>
		        </div>
                <div class="clear"></div>	                        
                <?php
                    $cat = 'cat=5&posts_per_page=-1';
                    query_posts( $cat );
                ?>
                <?php if (have_posts()) : ?>
                <div class="gal">
                   <div class="content-galeria">
                       <?php  while (have_posts()) : the_post();  ?> 
                       <?php
					   		// count views 
                            $counter = get_post_meta(get_the_ID(), '_post_views'); 
                            $views = 0 + $counter[0];
							
                            //$type = "fotos";
                            $subCategory = get_the_category();
                            foreach ($subCategory as $subCat) {
                                if ($subCat->parent == 5) { $sub = $subCat; }
                            }

                            // parse gallery
                            $urls = array();
                            $gallery = get_field('galeria', false);
                            if (sizeof($gallery) > 1) {
                                foreach ($gallery as $photo) { 
                                    $urls[] = sanitize_path($photo['url']);
                                }
                            } else {
                                $urls[] = sanitize_path(get_field('foto_principal'));   
                            }
							$surls = implode("|",$urls);
						?>
						<?php if ($surls != "") : ?>
                        <div class="item <?php echo $sub->slug." fotos"; ?>">
                            <a href="#" rel="<?php echo get_the_ID(); ?>" data-video="" data-gallery="<?php echo implode("|",$urls); ?>">
                            	<?php if(get_field('foto_miniatura') != ''){ ?>
                                <img src="<?php echo sanitize_path(get_field('foto_miniatura')); ?>" width="300" height="190" class="img-zoom">
                           	 	<?php  } else { ?>
                                <img src="<?php echo sanitize_path($urls[0]); ?>" width="300" height="190" class="img-zoom">
                            	<?php } ?>
                            </a>
                            <div class="info">
                                <h2><?php echo $sub->name; ?></h2>
                                <p><?php the_title(); ?></p>
                                <span class="fecha"><?php echo get_post_time(); ?></span>
                                <span class="visto"><?php echo $views; ?></span>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <?php endif; ?>
                        
						<?php
                            // parse video
                            $idvideo = "";
                            $video = get_field('video');
                            if ($video != "") :
                                preg_match('/src="(.+?)"/', $video, $matches);
								$src = $matches[1];
								if (strpos($src,'youtube') !== false) {
									$src = explode('https://www.youtube.com/embed/', $src);
									$idvideo = explode('?feature=oembed', $src[1]);
									$idvideo = $idvideo[0];
									$thumbvideo = getYoutubeThumb($idvideo);
									$idvideo = "youtube-".$idvideo;
                                    console.log('las cosas estan lentas');
								} else {
									if (strpos($src,'vimeo') !== false) {
										$urlParts = explode("/", parse_url($src, PHP_URL_PATH));
										$idvideo = $urlParts[count($urlParts)-1];
										$thumbvideo = getVimeoThumb($idvideo);
										$idvideo = "vimeo-".$idvideo;
									}
								}
                                //$type = "videos";        
                        ?>
                       <div class="item <?php echo $sub->slug." videos"; ?>">
                            <a href="#" rel="<?php echo get_the_ID(); ?>" data-video="<?php echo $idvideo; ?>" data-gallery="">
                            	<img src="<?php echo $thumbvideo; ?>" width="300" height="190" class="img-zoom">
                            </a>
                            <div class="info">
                                <h2><?php echo $sub->name; ?></h2>
                                <p><?php the_title(); ?></p>
                                <span class="fecha"><?php echo get_post_time(); ?></span>
                                <span class="visto"><?php echo $views; ?></span>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>
                
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>

<?php get_footer(); ?>
