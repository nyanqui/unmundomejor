<?php get_header(); ?>
<?php  while (have_posts()) : the_post();  ?>
	<div class="contenido-box">
		<div class="contenedor-contacto">
			<div class="text-contact">
				<h1>Contáctanos</h1>
				<p>Completa el formulario. Nos contactaremos muy pronto contigo.</p>
			</div>
			<div class="form-contact">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
<?php endwhile; ?>
<?php get_footer(); ?>