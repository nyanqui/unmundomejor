<?php
/* customize wp-header */
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action( 'wp_head', 'rel_canonical'); // remove canonical
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10,0);

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


add_filter('show_admin_bar', '__return_false');
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 825, 510, true );

function get_template_path() {
	return '/unmundomejor/wp-content/themes/lan-template';	
}
function get_main_path($path = "") {
	if ($path != "") {
		return "/unmundomejor/" . $path;
	} else {
		return '/unmundomejor';	
	}
}
function sanitize_path($path) {
	//$path = str_replace("http://lanenperu.pe", "", $path);
	//$path = str_replace("https://lanenperu.pe", "", $path);
	$path = str_replace("http://34.192.168.138", "", $path);
	$path = str_replace("https://34.192.168.138", "", $path);
	
	return $path;
}

function get_relative_permalink( $url = '' ) {
    $url = get_permalink();
    return str_replace( home_url(), "/unmundomejor", $url );
}


function fmtYoutubeFrame($iframe) {
    preg_match('/src="(.+?)"/', $iframe, $matches);
    $src = $matches[1];

	if (strpos($src,'youtube') !== false) {
		// youtube player
		$params = array(
			'controls'    => 1,
			'hd'        => 1,
			'autohide'    => 1,
			'showinfo'    => 0,
		);
	    $new_src = add_query_arg($params, $src);
    	$iframe = str_replace($src, $new_src, $iframe);
	} else {
		if (strpos($src,'vimeo') !== false) {
			$params = array(
				'title'    => 0,
				'portrait' => 0,
				'byline'   => 0,
				'badge'	   => 0
			);
			$new_src = add_query_arg($params, $src);
			$iframe = str_replace($src, $new_src, $iframe);
		}
	}
    // add extra attributes to iframe html
    $attributes = 'frameborder="0"';

    $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
    return $iframe;
}
function fmtYoutubeSrc($iframe) {
	$idvideo = "";
	$src = "";
	$video = get_field('video');
	if ($video != "") {
		preg_match('/src="(.+?)"/', $iframe, $matches);
		$src = explode('https://www.youtube.com/embed/', $matches[1]);
		$idvideo = explode('?feature=oembed', $src[1]);
		$idvideo = $idvideo[0];
		$src = 'https://www.youtube.com/watch?v='.$idvideo;
	}
    return $src;
}
function getVimeoThumb($id) {
    $data = file_get_contents("http://vimeo.com/api/v2/video/$id.json");
    $data = json_decode($data);
    return $data[0]->thumbnail_medium;
}
function getYoutubeThumb($id) {
	$data = "http://img.youtube.com/vi/$id/hqdefault.jpg";
	return $data;
}

// add post-formats to post_type 'video'
add_action('init', 'my_custom_init');
function my_custom_init() {
	add_post_type_support( 'video', 'post-formats' );
}

// remove x-pingback
function remove_x_pingback($headers) {
    unset($headers['X-Pingback']);
    return $headers;
}
add_filter('wp_headers', 'remove_x_pingback');

remove_action( 'template_redirect', 'wp_shortlink_header', 11 );


function boxblue_function( $atts, $content = null ) {
   return '<div class="box-publicaciones">
              <div class="right">
                <p class="publi-frase-css">'.do_shortcode($content).'</p>
              </div>
            </div>';
}
add_shortcode('frasepost', 'boxblue_function');
//[frasepost] [/frasepost]


function column_shortcode( $atts, $content = null ) {
	$parts = array();
	$parts = explode(' | ', $content);

	$a = shortcode_atts( array(
	'side' => 'left',
	), $atts );

	$html = '<div class="box-publicaciones">';
	$html .= ($a['side'] == "left") ? '<div class="left">' : '<div class="right">';
	$html .= $parts[0];
	$html .= '</div>';
	$html .= ($a['side'] == "left") ? '<div class="right">' : '<div class="left">';
	$html .= $parts[1];
	$html .= '</div>';
	$html .= '</div>';

	return $html;
	}

add_shortcode('columnapost', 'column_shortcode');
//[columnapost side="left|right"] texto | texto2 [/columnapost]