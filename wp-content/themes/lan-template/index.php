﻿<?php get_header(); ?>

<div class="popup-galeria">
    <div class="bg"></div>
    <div class="galeria">
        <div class="carousel-galeria">
            <ul>
            <li><?php echo fmtYoutubeFrame(get_field('video')); ?></li>
            </ul>
        </div>
        <div class="controls">
            <!-- <a href="#" class="fullscreen"><img src="<?php echo get_template_path(); ?>/img/fullscreen.gif"></a>
            <a href="#" class="arrowprev2"><img src="<?php echo get_template_path(); ?>/img/arrowleft2.gif"></a>
            <a href="#" class="arrownext2"><img src="<?php echo get_template_path(); ?>/img/arrowright2.gif"></a>
            <div class="counter"><span class="first">1</span>/<span class="second">7</span></div> -->
        </div>
        <a href="#" class="regresar"><img src="<?php echo get_template_path(); ?>/img/backarrow2.png">Regresar</a>
    </div>
</div>

<div class="page-home">
    <div class="inicio">
        <div style="background: transparent;" class="block block1 intro intro-box-xs">
            <h1>Un mundo mejor</h1>
            <h2>Descubre las actividades de <span>RESPONSABILIDAD SOCIAL</span> que realizamos para tener un mundo mejor.</h2>
            <!--h3 class="desktop">O, si deseas, puedes ver nuestro video. <img src="<?php echo esc_url( get_template_path() ); ?>/img/icon-video.gif"></h3>
            <h3 class="movil"><a href="<?php echo fmtYoutubeSrc(get_field('video')); ?>" target="_blank">O, si deseas, puedes ver nuestro video. <img src="<?php echo esc_url( get_template_path() ); ?>/img/icon-video.gif"></a></h3-->
        </div>
        <div class="block block2">
            <ul>
                <li>
                    <a href="<?php echo get_main_path(); ?>/iniciativas/turismo-sostenible/" class="turismo">
                        <div class="normal">
                            <img src="<?php echo esc_url( get_template_path() ); ?>/img/turismo-sostenible.jpg" class="bg">
                        </div>
                        <div class="titulo">Turismo<br>Sostenible</div>
                        <div class="arrow"><img src="<?php echo esc_url( get_template_path() ); ?>/img/arrow.png"></div>
                        <div class="over">
                            <img src="<?php echo esc_url( get_template_path() ); ?>/img/turismo-sostenible-on.jpg" class="bg">
                            <div class="texto">Incentivamos y<br>promovemos el<br>Turismo Sostenible.</div>
                            <div class="vermas">Ver más</div>
                        </div>
                    </a>
                </li>
                <li class="central">
                    <a href="<?php echo get_main_path(); ?>/iniciativas/comunidad/" class="comunidad">
                        <div class="normal">
                            <img src="<?php echo esc_url( get_template_path() ); ?>/img/comunidad.jpg" class="bg">
                        </div>
                        <div class="titulo">Comunidad</div>
                        <div class="arrow"><img src="<?php echo esc_url( get_template_path() ); ?>/img/arrow.png"></div>
                        <div class="over">
                            <img src="<?php echo esc_url( get_template_path() ); ?>/img/comunidad-on.jpg" class="bg">
                            <div class="texto">Conoce nuestros<br>programas de<br>Responsabilidad Social.</div>
                            <div class="vermas">Ver más</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo get_main_path(); ?>/iniciativas/cambio-climatico/" class="medioambiente">
                        <div class="normal">
                            <img src="<?php echo esc_url( get_template_path() ); ?>/img/cambio-climatico.jpg" class="bg">
                        </div>
                        <div class="titulo">Cambio<br>Climático</div>
                        <div class="arrow"><img src="<?php echo esc_url( get_template_path() ); ?>/img/arrow.png"></div>
                        <div class="over">
                            <img src="<?php echo esc_url( get_template_path() ); ?>/img/cambio-climatico-on.jpg" class="bg">
                            <div class="texto">Nos preocupamos por<br>minimizar nuestro<br>impacto ambiental.</div>
                            <div class="vermas">Ver más</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="categorias-movil">
        	<a href="<?php echo get_main_path(); ?>/iniciativas/turismo-sostenible/"><img src="<?php echo esc_url( get_template_path() ); ?>/img/btn-home-turismo-sostenible.jpg"></a>
        	<a href="<?php echo get_main_path(); ?>/iniciativas/comunidad/" class="central"><img src="<?php echo esc_url( get_template_path() ); ?>/img/btn-home-comunidad.jpg"></a>
        	<a href="<?php echo get_main_path(); ?>/iniciativas/cambio-climatico/"><img src="<?php echo esc_url( get_template_path() ); ?>/img/btn-home-cambio-climatico.png"></a>
        </div>
        <div class="block block3">

        </div>
    </div>
    <div class="clear"></div>
</div>

<?php get_footer(); ?>
