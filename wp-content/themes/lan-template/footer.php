	</div>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer">
			<div class="content">
				<div class="copy">Un mundo mejor</div>
				<ul class="nav">
					<li><a href="<?php echo get_main_path( '/' ); ?>" class="nav-inicio">Inicio</a></li>
					<li class="central"><a href="<?php echo get_main_path( 'iniciativas/turismo-sostenible/' ); ?>" class="nav-iniciativas">Turismo sostenible</a></li>
					<li><a href="<?php echo get_main_path( 'iniciativas/comunidad/' ); ?>" class="nav-iniciativas">Comunidad</a></li>
					<li class="central"><a href="<?php echo get_main_path( 'iniciativas/cambio-climatico/' ); ?>" class="nav-iniciativas">Cambio Climático</a></li>
					<li><a href="<?php echo get_main_path( 'galeria/' ); ?>" class="nav-galeria">Galería</a></li>
				</ul>
				<ul class="social">
					<li><a href="https://www.facebook.com/LATAMPeru/" target="_blank" class="fb"></a></li>	
					<li class="central"><a href="https://www.youtube.com/user/lanairlines" target="_blank" class="yt"></a></li>
					<li><a href="https://plus.google.com/u/1/+LANenPeru/posts" target="_blank" class="go"></a></li>
				</ul>
				<p>© 2017 NUESTRO PRÓXIMO UN MUNDO MEJOR, una división de LATAM Airlines Perú, Inc.</p>
			</div>
		</div>
	</footer>
	<div class="clear"></div>
</div>

<?php wp_footer(); ?>

<script src="<?php echo esc_url( get_template_path() ); ?>/js/jquery.scrolldepth.min.js" type="text/javascript"></script>
<script src="<?php echo esc_url( get_template_path() ); ?>/js/jquery.scrollTo.min.js" type="text/javascript"></script>
<script src="<?php echo esc_url( get_template_path() ); ?>/js/jquery.jcarousellite.min.js" type="text/javascript"></script>
<script src="<?php echo esc_url( get_template_path() ); ?>/js/jquery.transform2d.js" type="text/javascript"></script>
<script src="<?php echo esc_url( get_template_path() ); ?>/owl-carousel/owl.carousel.js" type="text/javascript"></script>
<script src="<?php echo esc_url( get_template_path() ); ?>/js/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="<?php echo esc_url( get_template_path() ); ?>/js/jquery.fitvids.js" type="text/javascript"></script>
<script src="<?php echo esc_url( get_template_path() ); ?>/js/lunametrics-youtube-v7.gtm.min.js"></script>
<script>
jQuery(document).ready(function($){
    $('.mundo').animate({transform: 'translateX(0) translateY(0%) rotate(-32deg)'},  0);
    $('.turismo').hover(function(){ $('.mundo').stop().animate({transform: 'translateX(0) translateY(0%) rotate(-32deg)'}); }, function(){ });
    $('.comunidad').hover(function(){ $('.mundo').stop().animate({transform: 'translateX(0) translateY(0%) rotate(32deg)'}); }, function(){ });
    $('.medioambiente').hover(function(){ $('.mundo').stop().animate({transform: 'translateX(0) translateY(0%) rotate(0deg)'}); }, function(){ });

	var j=1;
	var total = $(".galeria .carousel-galeria div").length;
	var owl = $(".galeria .carousel-galeria");
	var cross = false;
	var j=0;
	owl.owlCarousel({
		pagination : false,
		paginationNumbers: false,
		singleItem:true,
		scrollPerPage:true,
		rewindNav:true
	});
	var owl1 = $(".galeria .carousel-galeria").data('owlCarousel');
		        
		$('.second').html(total);
		$('.arrownext2').click(function(e){
			e.preventDefault();
			owl.trigger('owl.next');
			if(j<total){
				j++;
			} else {
				j=1;
			}
			$('.first').html(j);
		});
		$('.arrowprev2').click(function(e){
			e.preventDefault();
			owl.trigger('owl.prev');
			if(j>1){
				j--;
			} else {
				j=total;
			}
			$('.first').html(j);
		});


        $('.moverabajo').click(function(e){
            e.preventDefault();
            $.scrollTo('.contenido .header-block', 350);
        });

/*        function Resize(){
            $('.page-galeria .contenido .left').css('height', parseInt($('.page-galeria .contenido .right').height()+75)+'px');
            $('.popup-galeria').css('height', parseInt($('.main').height())+'px');
        }*/

        // $(window).on('resize', function(){ Resize(); });
        // $(window).on('load', function(){ Resize(); });

		var post;
		$('.popup-galeria .regresar, .popup-galeria .bg').click(function(e){
			e.preventDefault();
			$('.popup-galeria').fadeOut(350);
		});

		$('.gal .item a').click(function(e){
			e.preventDefault();
   
			post = $(this).attr('rel');
			imgs = $(this).attr('data-gallery');
			video = $(this).attr('data-video');

			if(video != '') {
				$('.popup-galeria .controls').hide();	
			}
			else {
				$('.popup-galeria .controls').show();
			}

			$.scrollTo('.main', 500, function(){
				$('.popup-galeria').fadeIn(350, function(){
					SearchCategory(post, imgs, video);
				});
			});
		});
		
		var total2=0;

		var owl2 = $("#owl-gallery");
		var owl4 = $("#owl-gallery").data('owlCarousel');
		var j=1;
		owl2.owlCarousel({
			pagination : false,
			paginationNumbers: false,
			singleItem:true,
			scrollPerPage:true,
			rewindNav:true
		});

		//Enviar búsqueda
		function SearchCategory(post, imgs, video){
			// show gallery
			for (var m=0; m<total; m++){
			   owl4.removeItem(m); 
			}
			total = 0;
			j = 1;
			
			$('#owl-gallery').html('');
			var content1, content2;
			var totSlides =0;
			
			if (imgs != "") {
				// parse and print gallery
				var $gallery = imgs.split("|");
				for (var i=0;i<$gallery.length;i++){
					content2 = '<div><img src="'+$gallery[i]+'" width="1024" height="559"></div>';
					owl2.data('owlCarousel').addItem(content2);
					totSlides++;
				}

				if(totSlides==1){
					$('.galeria .arrowprev3, .galeria .arrownext3').css('opacity', .2);
					$('.galeria .arrowprev3, .galeria .arrownext3').css('cursor', 'default');
				} else {
					$('.galeria .arrowprev3, .galeria .arrownext3').css('opacity', 1);
					$('.galeria .arrowprev3, .galeria .arrownext3').css('cursor', 'pointer');
				}

				$('.first').html('1');
				$('.second').html('');
			}
			
			if (video != "") {
				// parse and print video
				var avideo = video.split("-");
				if (avideo[0] == "youtube") {
					content1 = '<div><iframe width="100%" height="100%"  src="https://www.youtube.com/embed/'+avideo[1]+'" frameborder="0" allowfullscreen></iframe></div>';
				} 
				if (avideo[0] == "vimeo") {
					content1 = '<div><iframe width="100%" height="100%"  src="https://player.vimeo.com/video/'+avideo[1]+'?title=0&amp;portrait=0&amp;byline=0&amp;badge=0" frameborder="0" title="Christina" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe></div>';
				} 
									
				owl2.data('owlCarousel').addItem(content1);
				$('iframe').css('height', parseInt($('#owl-gallery').width()*17.6)/30+'px');
				totSlides++;
			}

            total = totSlides;
            $('.second').html(total);
		}

		$('.galeria .arrownext3').click(function(e){
			e.preventDefault();
			owl2.trigger('owl.next');

			if(j<total){
				j++;

			} else {
				j=1;
			}
			$('.first').html(j);
		});
		$('.galeria .arrowprev3').click(function(e){
			e.preventDefault();
			owl2.trigger('owl.prev');
			if(j>1){
				j--;
			} else {
				j=total;
			}
			$('.first').html(j);
		});
 
		$('.inicio h3.desktop').click(function(e){
			e.preventDefault();
			$('.popup-galeria').fadeIn(350);
			ga('send', 'event', 'home', 'click', 'ver-video');

		});
		$('.popup-galeria .regresar, .popup-galeria .bg').click(function(e){
			e.preventDefault();
			$('.popup-galeria').fadeOut(350, function() {
				//$('.popup-galeria .carousel-galeria ul').html('');
				$('#owl-gallery').html('');
			});
		});

            var $container = $('.container').eq(0).isotope({
			    itemSelector: '.item',
			    layoutMode: 'fitRows',
			    getSortData: {
			      fecha: '.fecha parseInt',
			      visto: '.visto parseInt'
			    },
			    sortAscending: false
			  });

			$('.orden .fecha:eq(0), .orden .relevancia:eq(0)').click(function(e) {
				e.preventDefault();
			    var sortByValue = $(this).attr('data-sort-by');
			    $container.isotope({ sortBy: sortByValue });

			    $('.orden a').removeClass('selected');
			    $(this).addClass('selected');
			  });

			var $container1 = $('.container').eq(1).isotope({
			    itemSelector: '.item',
			    layoutMode: 'fitRows',
			    getSortData: {
			      fecha: '.fecha parseInt',
			      visto: '.visto parseInt'
			    },
			    sortAscending: false
			  });

			$('.orden .fecha:eq(1), .orden .relevancia:eq(1)').click(function(e) {
				e.preventDefault();
			    var sortByValue = $(this).attr('data-sort-by');
			    $container1.isotope({ sortBy: sortByValue });

			    $('.orden a').removeClass('selected');
			    $(this).addClass('selected');

			  });

			$('.img-zoom').hover(function() {
		        $(this).addClass('transition');
		 
		    }, function() {
		        $(this).removeClass('transition');
		    });

			// Galería 
            var cat_active = '*';
            var sort_active;

			var $containergal;
			$(window).load(function(){
				$containergal = $('.content-galeria').isotope({
					itemSelector: '.item',
					layoutMode: 'fitRows',
					getSortData: {
					  fecha: '.fecha parseInt',
					  visto: '.visto parseInt'
					},
					sortAscending: false
				  });

			$('.page-galeria .orden .fecha, .page-galeria  .orden .relevancia').click(function(e) {
				e.preventDefault();
			    var sortByValue = $(this).attr('data-sort-by');
			    $containergal.isotope({ sortBy: sortByValue });

			    $('.orden a').removeClass('selected');
			    $(this).addClass('selected');
                
                
			  });

			 // filter functions
				  

			  // bind filter button click
			  $('.menu-lat li a').click(function(e) {
			  	e.preventDefault();
			    var filterValue = $( this ).attr('data-filter-by');
			    $containergal.isotope({ filter: filterValue });
			    $('.menu-lat li a').removeClass('selected');
			    $(this).addClass('selected');
                cat_active = filterValue;
   
			  });

			  $('.right .headers .links a').click(function(e) {
			  	e.preventDefault();
			    var filterValue = $( this ).attr('data-filter-by');
				var txtfilter = filterValue+cat_active;
				if (filterValue==cat_active) { txtfilter = filterValue; } 
			    $containergal.isotope({ filter: txtfilter });
			    $('.right .headers .links a').removeClass('selected');
			    $(this).addClass('selected');
                sort_active = filterValue;
			  });

			  $('.right .headers-movil .links a').click(function(e) {
			  	e.preventDefault();
			    var filterValue = $( this ).attr('data-filter-by');
			    $containergal.isotope({ filter: filterValue });
			    $('.right .headers-movil .links a').removeClass('selected');
			    $(this).addClass('selected');
			  });

			  $(".contenido-post .pulldown select:eq(0)").each(function (i, e) {
	                var $pull = $(this);
	                $pull.change(function () {
	                    $pull.parent().children("div.txtpull").find('span').text($pull.children(":selected").text());
	                    var sortByValue = $pull.children(":selected").val();
					    $container.isotope({ sortBy: sortByValue });
	                });
	            });

			  $(".contenido-post .pulldown select:eq(1)").each(function (i, e) {
	                var $pull = $(this);
	                $pull.change(function () {
	                    $pull.parent().children("div.txtpull").find('span').text($pull.children(":selected").text());
	                    var sortByValue = $pull.children(":selected").val();
					    $container1.isotope({ sortBy: sortByValue });
	                });

	            });

			  $(".page-galeria .pulldown:eq(0)").each(function (i, e) {
	                var $pull = $(this);
	                $pull.change(function () {
	                    $pull.parent().find('.txtpull span').text($pull.find(":selected").text());
	                    var sortByValue = $pull.find(":selected").val();

					    $containergal.isotope({ filter: sortByValue });

	                });

	            });
			  $(".page-galeria .pulldown:eq(1)").each(function (i, e) {
	                var $pull = $(this);
	                $pull.change(function () {
	                    $pull.parent().find('.txtpull span').text($pull.find(":selected").text());
	                    var sortByValue = $pull.find(":selected").val();

					    $containergal.isotope({ sortBy: sortByValue });

	                });

	            });


              ( function($) {
                    $.fn.responsiveVideo = function() {
                        // Add CSS to head
                        $( 'head' ).append( '<style type="text/css">.responsive-video-wrapper{width:100%;position:relative;padding:0}.responsive-video-wrapper iframe,.responsive-video-wrapper object,.responsive-video-wrapper embed{position:absolute;top:0;left:0;width:100%;height:100%}</style>' );
                        // Gather all videos
                        var $all_videos = $(this).find( 'iframe[src*="player.vimeo.com"], iframe[src*="youtube.com"], iframe[src*="youtube-nocookie.com"], iframe[src*="dailymotion.com"], iframe[src*="kickstarter.com"][src*="video.html"], object, embed' );
                        // Cycle through each video and add wrapper div with appropriate aspect ratio if required
                        $all_videos.not( 'object object' ).each( function() {
                            var $video = $(this);
                            if ( $video.parents( 'object' ).length )
                                return;
                            if ( ! $video.prop( 'id' ) )
                                $video.attr( 'id', 'rvw' + Math.floor( Math.random() * 999999 ) );
								
							var ptop = $video.attr('height')/$video.attr('width');
                            $video
                                .wrap( '<div class="responsive-video-wrapper" style="padding-top: ' + ( ptop*100 ) + '%" />' )
                                .removeAttr( 'height' )
                                .removeAttr( 'width' );
                        } );
                    };
                } )(jQuery);

                $( 'body' ).responsiveVideo();

			});
			
    $('.footer .nav-inicio').click(function(){
        ga('send', 'event', 'home-footer', 'click', 'inicio');
    });
    $('.footer .nav-iniciativas').click(function(){
        ga('send', 'event', 'home-footer', 'click', 'iniciativa');
    });
    $('.footer .nav-galeria').click(function(){
        ga('send', 'event', 'home-footer', 'click', 'galeria');
    });

    $('.footer .fb').click(function(){
        ga('send', 'event', 'home-footer-rs', 'click', 'facebook');
    });
    $('.footer .yt').click(function(){
        ga('send', 'event', 'home-footer-rs', 'click', 'youtube');
    });
    $('.footer .go').click(function(){
        ga('send', 'event', 'home-footer-rs', 'click', 'google-plus');
    });

    $('.menu-expanded .fb').click(function(){
        ga('send', 'event', 'navegador', 'click', 'rss-fb');
    });
     $('.menu-expanded .yt').click(function(){
        ga('send', 'event', 'navegador', 'click', 'rss-youtube');
    });
      $('.menu-expanded .go').click(function(){
        ga('send', 'event', 'navegador', 'click', 'rss-gplus');
    });


      function throttle(func, wait) {
          var context, args, result;
          var timeout = null;
          var previous = 0;
          var later = function() {
            previous = new Date;
            timeout = null;
            result = func.apply(context, args);
          };
          return function() {
            var now = new Date;
            if (!previous) previous = now;
            var remaining = wait - (now - previous);
            context = this;
            args = arguments;
            if (remaining <= 0) {
              clearTimeout(timeout);
              timeout = null;
              previous = now;
              result = func.apply(context, args);
            } else if (!timeout) {
              timeout = setTimeout(later, remaining);
            }
            return result;
          };
        }

        $(window).load(function() {
          var $window = $(window),
          docHeight = $(document).height(),
            winHeight = window.innerHeight ? window.innerHeight : $window.height(),
            scrollDistance = $window.scrollTop() + winHeight,
            cache = [];

            var q = docHeight / 4;

            $('.q').css({ height: q })
            $('#q1').css({ top: 0 });
            $('#q2').css({ top: q + 'px' });
            $('#q3').css({ top: q * 2 + 'px' });
            $('#q4').css({ bottom: 0 });

            //$('.q').css({ display: 'block'});

            function calculateMarks(docHeight) {
              return {
                '25%' : parseInt(docHeight * 0.25, 10),
                '50%' : parseInt(docHeight * 0.50, 10),
                '75%' : parseInt(docHeight * 0.75, 10),
                // 1px cushion to trigger 100% event in iOS
                '100%': docHeight - 5
              };
            }

            function checkMarks(marks, scrollDistance) {

              $.each(marks, function(key, val) {
                if ( $.inArray(key, cache) === -1 && scrollDistance >= val ) {

                  setTimeout(function() {
          
                    ga('send', 'event', '<?php echo get_the_title(); ?>', 'scroll', key);
                    
                    cache.push(key);
                  }, 250);

                }
              });
            }


        $window.on('scroll.scrollDepth', throttle(function() {

          var $window = $(window),
          docHeight = $(document).height(),
            winHeight = window.innerHeight ? window.innerHeight : $window.height(),
            scrollDistance = $window.scrollTop() + winHeight;

          // Recalculate percentage marks
          var marks = calculateMarks(docHeight);

          // If all marks already hit, unbind scroll event
          if (cache.length >= 4) {
            $window.off('scroll.scrollDepth');
            return;
          }

          checkMarks(marks, scrollDistance);

          }, 500));

        });
		




}); 
</script>
<script>
    var FBID, accessToken, uid, vNombre;

    window.fbAsyncInit = function() {
      FB.init({
        appId      : '117352465265919',
        status     : true,
        xfbml      : true
      });

        FB.Canvas.setSize({
            height: 800
        });

      function shareOnFacebook(name, link, caption, picture, description){
            FB.ui({
                method: 'feed',
                name: name,
                link: link,
                caption: caption,
                picture: picture,
                description: description
            }, function(response) {
                if(response && response.post_id){

                }
                else{

                }
            });  
        };

      jQuery(document).ready(function($){

         $('.fb').click(function(e){
                e.preventDefault();
                //var img = $(this).parent().find('img').attr('src');
				var img = $('.banner2 ul li img').attr('src');
				if (img == undefined ) {
					img = 'http://www.lan.com' + $(this).attr("data-img");	
				} else {
					img = 'http://www.lan.com' + img;	
				}
                shareOnFacebook($(this).attr("data-title") + " - LAN Responsabilidad Social", $(this).attr("data-url"), '', img, $(this).attr("data-text"));
                ga('send', 'event', $(this).attr("data-key"), 'click', 'Compartir Facebook');

            });

    });

    };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

</script>
<script src="//cdn.cxtn.net/loader/loader.js#channelid=5559e246cbd4229c74000011" type="text/javascript" ></script>
<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45906942-3', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>
</body>
</html>
